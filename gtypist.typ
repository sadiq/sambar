# Adapted from gtypist q.typ
# Written by Mohammed Sadiq <www.sadiqpk.org>
# Copyright is the same as gtypist
# run with: gtypist gtypist.typ

# Changes made:
# * The lines are made shorter so that gtypist won't
#   close on smaller screens (like Termux)
# * More lessons are added that would help learn
#   writing code


#------------------------------------------------------------------------------
# Series Q
#------------------------------------------------------------------------------
G:_Q_MENU
*:_Q_NO_MENU

#------------------------------------------------------------------------------
# Lesson Q1
#------------------------------------------------------------------------------
*:Q1
*:_Q_S_Q1
B:                             Lesson Q1

*:_Q_R_L1
T:			The HOME Keys.
 :
 :Place the first finger of your right hand on the J-key,
 :2nd on K, 3rd on L, and 4th on the ;-key.

*:_Q_R_L2
T:			The HOME Keys.
 :
 :Similarly place your left hand on the F, D, S, and A-keys.
 :Place your right thumb over the SPACE bar.

I:(1) Try this:
*:_Q_R_L3
D:asdf ;lkj asdf ;lkj asdf ;lkj asdf ;lkj asdf ;lkj asdf ;lkj

I:Now this (use 'k' finger for 'i' and 'd' finger for 'e'):
*:_Q_R_L4
D:asdef ;lkij asdef ;lkij asdef ;lkij asdef ;lkij asdef ;lkij

I:(2) Some more:
*:_Q_R_L5
D:as al ad ak af aj fa ka da la sa ja sl sd sk sf ls ds ks fs

I:(3) Hang in there; let's do some sentences...
 :To get capitals use your ';' finger on [right-SHIFT]
*:_Q_R_L6
D:Dad adds a salad  A lad asks  Salad falls as a lad asks Dad

I:(4) Now try [left-SHIFT] usage (for 'L') and [right-SHIFT]!
*:_Q_R_L7
D:Lease a desk  Add a safe deal  Ask less fees  Add a lease

I:(5)
*:_Q_R_L8
D:Feel a dead faded leaf  Seeds fall as a faded leaf falls

I:(6)
*:_Q_R_L9
D:Idle Sid seeks a salad  Sis aids Sid  A salad is laid aside

I:(7)
*:_Q_R_L10
D:Sails fill as Sis sails a safe lake  Skill aids Sis  Dad

I:(8)
*:_Q_R_L10.1
D:likes a safe sail  Sis seeks a lee isle  All sail is

I:(9)
*:_Q_R_L10.2
D:Faded sails fill  Idle isles slide aside as Sis sails

I:(10)
*:_Q_R_L11
D:Sid adds all sail as Dad sees a safe sea as idle as a lake

G:_Q_E_Q1

#------------------------------------------------------------------------------
# Lesson Q2
#------------------------------------------------------------------------------
*:Q2
*:_Q_S_Q2
B:                             Lesson Q2

*:_Q_R_L12
T:In this lesson you shall learn (H, G, O, U, N, T, .)
 :
 :Fingers:'f' for 't' and 'g', 'j' for 'h', 'u' and 'n',
 :and 'l' for 'o' and '.'

I:(1) Rhythm Drill
*:_Q_R_L13
D:a;sldkfjgh a;sldkfjgh a;sldkfjgh a;sldkfjgh a;sldkfjgh

I:(2) Rhythm Drill
*:_Q_R_L13.1
D:asdefghk lokijujhjn asdefghk lokijujhjn asdefghk

I:(3) Balanced Keyboard Drill
*:_Q_R_L14
D:as os es us is an on en un in at ot et ut it ad od ed ud id

I:(4) Continuous Copy
*:_Q_R_L15
D:Ed had a shed.  His shed had dishes.  He had shade.

I:(5)
*:_Q_R_L16
D:Odd ideas die like odd seeds.  Odd seeds die as do odd deeds.

I:(6)
*:_Q_R_L17
D:Sid used us.  Sid sued us.  Ada used us as aid.  I did aid.

I:(7)
*:_Q_R_L18
D:Ed is staid.  Ed uses tested data as assets.  Sis is a tease.

I:(8)
*:_Q_R_L19
D:Sis said Dean is dense as sand.  Dean needs an idea and Sis
 :needs a sedan.

I:(9) Rhythmic Review
*:_Q_R_L20
D:He sees that in a test he has to state and use a sane idea.

G:_Q_E_Q2

#------------------------------------------------------------------------------
# Lesson Q3
#------------------------------------------------------------------------------
*:Q3
*:_Q_S_Q3
B:                             Lesson Q3

*:_Q_R_L21
T:			  (y r c , ? : p)
 :
 : 'j' for 'y', 'f' for 'r', 'k' for ',', 'd' for 'c',
 : ';' for '?', 'p' and ':'

I:(1) Rhythm Drill
*:_Q_R_L22
D:deki frju dck, dcl. frju ftjy deki frju dck, dcl. frju ftjy

I:(2) Rhythm Drill
*:_Q_R_L22.1
D:fgjh ;p;? jujy dedc lol. kik, fgju ;:;: frfk jujy dedc kik,

I:(3) Balanced Keyboard Drill
*:_Q_R_L23
D:ag ac ar al ap at ay af ug uc ur ul up ut eg ec er el ep et

I:(4) Continuous Copy
*:_Q_R_L24
D:Chance can aid a nice choice.  It can teach one to count his
 :costs too.

I:(5)
*:_Q_R_L25
D:At his age a good song is the thing as he gets his dog and
 :gun.

I:(6)
*:_Q_R_L26
D:As soon as papa is deep in a nap Pat happens to pound in his
 :shop and the phone sounds.

I:(7)
*:_Q_R_L27
D:I hear there is an error in her other order.  The store sent
 :her red dress to our door.

I:(8)
*:_Q_R_L28
D:I shall hold those ideal hotel lots at least until all land
 :is sold.

I:(9)
*:_Q_R_L29
D:Sunday is too soon.  It is not easy to stay and study this
 :dandy day.

I:(10)
*:_Q_R_L30
D:One needs to use faith if one fishes often.  It is fun to sit
 :on soft sod and fish.

I:(11)
*:_Q_R_L31
D:Hello, is this Dan?  Hello, Dan, this is Ann.

I:(12) Rhythmic Review
*:_Q_R_L32
D:Papa can not plan to get us all there in such a car as this.

G:_Q_E_Q3

#------------------------------------------------------------------------------
# Lesson Q4
#------------------------------------------------------------------------------
*:Q4
*:_Q_S_Q4
B:                             Lesson Q4

*:_Q_R_L33
T:			(m w v z x b q ' -)
 :
 :Fingers: 'j' for 'm', 's' for 'w' and 'x',
 :'f' for 'v' and 'b' ';' for ' and '-', 'a' for 'z' and 'q'

I:(1) Rhythm Drill
*:_Q_R_L34
D:dedc kik, frfv jujm swsx lol. aqaz ;p;p frfv jujm ftfb jyjn

I:(2) Rhythm Drill
*:_Q_R_L34.1
D:aqsw az;p sxl. fvjm fvjn fbjn aqsw az;p sxl. fvjm fvjn fbjn

I:(3) Balanced Keyboard Drill
*:_Q_R_L35
D:am aw av az ak ax ab um ub em ew ev ez ek eq ex om ow ov oz

I:(4) Continuous Copy
*:_Q_R_L36
D:Iowa was white with snow when we two went down town and saw
 :a show.

I:(5)
*:_Q_R_L37
D:John has to use a tan and jet auto.  He joined Jane in its
 :joint use.

I:(6)
*:_Q_R_L38
D:Smith is his name.  He is on some Maine team.  I am to meet
 :him and Miss Smith.

I:(7)
*:_Q_R_L39
D:Kate uses ink to send a note south to Kansas kin.

I:(8)
*:_Q_R_L40
D:I advise Eva in vain to avoid an auto visit in seven states.

I:(9)
*:_Q_R_L41
D:She has questions and unique ideas to quote us.

I:(10)
*:_Q_R_L42
D:The zoo is shut.  His zest is dashed.  Dan dozes.  One sneeze
 :and then a dozen seize Dan.

I:(11)
*:_Q_R_L43
D:The boat has been best to Boston.

I:(12)
*:_Q_R_L44
D:Nan is in Texas.  She is anxious to dine at six.

I:(13) Rhythmic Review
*:_Q_R_L45
D:Ask them to let us have the car if they both go to the show.

G:_Q_E_Q4

#------------------------------------------------------------------------------
# Lesson Q5
#------------------------------------------------------------------------------
*:Q5
*:_Q_S_Q5
B:                             Lesson Q5

*:_Q_R_L46
T:
 :Now you know all of the alphabet.  In this lesson we add the
 :hyphen (-) and the apostrophe (').

I:(1) Rhythm Drill
*:_Q_R_L47
D:dedc kik, frfv jujm swsx lol. aqaz ;p;p frfv jujm ftfb jyjn

I:(2) Rhythm Drill
*:_Q_R_L47.1
D:frfk fvfb jujy jmjn aqsw azsw azsx ;plo ;p;- kik, ;p;-

I:(3) Balanced Keyboard Drill
*:_Q_R_L48
D:ad ar an al am ab ee st ed er en el es em ex om on or un up

I:(4) Continuous Copy -- Review
*:_Q_R_L49
D:It's a good thing papa has gone.  Pat gets up a deep song.

I:(5)
*:_Q_R_L50
D:They often need funds but don't think it is any fun to study.

I:(6)
*:_Q_R_L51
D:Ted notes an odd noise.  Dan is in the seas and needs aid.

I:(7)
*:_Q_R_L52
D:A good visit East is Ann's next quest.  Ann seems to seize
 :on this idea with zest.

I:(8)
*:_Q_R_L53
D:She is to adjust her six visits to have a snow-white Maine
 :Christmas.

I:(9)
*:_Q_R_L54
D:It's a tax on time, but it's quite a new zone to Ann who is
 :in just the mood to end her quota of visits.

I:(10) Rhythmic Review
*:_Q_R_L55
D:Two of the boys are to do it today and two of them next week.

G:_Q_E_Q5

#------------------------------------------------------------------------------
# Lesson Q6
#------------------------------------------------------------------------------
*:Q6
*:_Q_S_Q6
B:                             Lesson Q6

*:_Q_R_L56
T:In this lesson we  shall learn <, >, /, ", and =.
 :
 :Use 'k' finger for < and 'l' finger for > and
 :';' finger for /.  Use LEFT-shift to ger < and >

I:(1)  Use ';' finger for =
*:_Q_R_L60
D:a = a - b; c--;

I:(2)
*:_Q_R_L61
D:<meta name="description" content="">

I:(3)
*:_Q_R_L57
D:<html><head><title>Hello world</title></head></html>

I:(4)
*:_Q_R_L58
D:<body><div class="card-header"></div></body>

I:(5)
*:_Q_R_L59
D:<ul class="list-unstyled text-small"> <li>Hi</li> </ul>

G:_Q_E_Q6


#------------------------------------------------------------------------------
# Lesson Q7
#------------------------------------------------------------------------------
*:Q7
*:_Q_S_Q7
B:                             Lesson Q7

*:_Q_R_L62
T:In this lesson we  shall learn (, ), {, and }.
 :
 :Use 'l' finger for ( and ';' finger for ).
 :Use ';' finger for { and }.  Also use LEFT-Shift
 :Use TAB key (with 'a' finger) for the spaces shown
 :at the beginning of lines

I:(1)
*:_Q_R_L63
D:int main(void){}

I:(2)
*:_Q_R_L64
D:puts("Hello world");

I:(3)
*:_Q_R_L65
D:len = strlen(word);

I:(4) Use 'l' finger for 9
*:_Q_R_L66
D:if (len > 9)
 :	puts("length is greater than 9");

G:_Q_E_Q7


#------------------------------------------------------------------------------
# Lesson Q8
#------------------------------------------------------------------------------
*:Q8
*:_Q_S_Q8
B:                             Lesson Q8

*:_Q_R_L67
T:In this lesson we  shall learn #, &, *, \ and :
 :
 :Use 'd' finger for # along with RIGHT-shift.
 :Use 'j' finger for & and 'k' finger for * along
 :with LEFT-shift

I:(1)
*:_Q_R_L68
D: #include <stdio.h>

I:(2)
*:_Q_R_L69
D:int main(void) {
 :	printf("Hello world\n");
 :}

I:(3) Use ; finger with LEFT-shift for :
*:_Q_R_L70
D:char *str = "Hello world";

I:(4)
*:_Q_R_L71
D:def main():
 :	print("Hello world")

I:(5)
*:_Q_R_L72
D:if (a && b) {
 :	puts("do something");
 :}

I:(6)
*:_Q_R_L73
D:struct Person<'a> {
 :	name: &'a str
 :}

I:(7)
*:_Q_R_L74
D:from microbit import *
 :
 :display.clear()

I:(8)
*:_Q_R_L75
D:display.scroll("Hello World")

G:_Q_E_Q8


#------------------------------------------------------------------------------
# Lesson Q9
#------------------------------------------------------------------------------
*:Q9
*:_Q_S_Q9
B:                             Lesson Q9

*:_Q_R_L76
T:In this lesson we  shall learn |, !, _, ?, %, +, [, and ]
 :
 : Use 'a' finger for !, 'f' finger for % along with RIGHT-shift.
 : Use ';' finger for |, ? _ and + along with LEFT-shift.
 : Use ';' finger for [, and ].

I:(1)
*:_Q_R_L77
D:ls -alh /

I:(2)
*:_Q_R_L78
D:sudo apt update && sudo apt install bash

I:(3)
*:_Q_R_L79
D:fn main() {
 :	println!("Are you okay?");
 :}

I:(4)
*:_Q_R_L80
D:if (a || b)
 :	printf("Do something\n");

I:(5) Use 's' finger for 2, and 'l' finger for 9
*:_Q_R_L81
D:while True:
 :	display.clear()
 :	sleep(222)
 :	display.set_pixel(2, 2, 9)

I:(6) Use 'f' finger for %
*:_Q_R_L82
D: printf("%s", "Hello world\n");

I:(7) Use 'a' finger for 1
*:_Q_R_L83
D:if (a + b > 10)
 :  printf("%s", "Hi!\n");

I:(8)
*:_Q_R_L84
D:fn get_name(&self) -> &'static str { self.name }

I:(9)
*:_Q_R_L85
D:assert_eq!(&empty_array, &[]);

I:(10)
*:_Q_R_L86
D:assert_eq!(&empty_array, &[]);

I:(11)
*:_Q_R_L87
D:cd ~/; touch test.txt && rm -f test.txt

G:_Q_E_Q9


#------------------------------------------------------------------------------
# Lesson series Q jump tables
#------------------------------------------------------------------------------
*:_Q_E_Q1
Q: Do you want to continue to lesson Q2 [Y/N] ? 
N:_Q_MENU
G:_Q_S_Q2
*:_Q_E_Q2
Q: Do you want to continue to lesson Q3 [Y/N] ? 
N:_Q_MENU
G:_Q_S_Q3
*:_Q_E_Q3
Q: Do you want to continue to lesson Q4 [Y/N] ? 
N:_Q_MENU
G:_Q_S_Q4
*:_Q_E_Q4
Q: Do you want to continue to lesson Q5 [Y/N] ? 
N:_Q_MENU
G:_Q_S_Q5
*:_Q_E_Q5
Q: Do you want to continue to lesson Q6 [Y/N] ? 
G:_Q_MENU
G:_Q_S_Q6
*:_Q_E_Q6
Q: Do you want to continue to lesson Q7 [Y/N] ? 
G:_Q_MENU
G:_Q_S_Q7
*:_Q_E_Q7
Q: Do you want to continue to lesson Q8 [Y/N] ? 
G:_Q_MENU
G:_Q_S_Q8
*:_Q_E_Q8
Q: Do you want to continue to lesson Q9 [Y/N] ? 
G:_Q_MENU
G:_Q_S_Q9
*:_Q_E_Q9
G:_Q_MENU

#------------------------------------------------------------------------------
# Lesson series Q menu
#------------------------------------------------------------------------------
*:_Q_MENU
B:                      Quick QWERTY course
M: UP=_EXIT "The Q series contains the following 9 lessons"
 :_Q_S_Q1 "Lesson Q1     a s d f j k l ; e i"
 :_Q_S_Q2 "Lesson Q2     h g o u n . t"
 :_Q_S_Q3 "Lesson Q3     y r c , ? : p"
 :_Q_S_Q4 "Lesson Q4     m w v z x b q"
 :_Q_S_Q5 "Lesson Q5     ' -"
 :_Q_S_Q6 "Lesson Q6     < > / ="
 :_Q_S_Q7 "Lesson Q7     ( ) { }"
 :_Q_S_Q8 "Lesson Q8     # & * \ :"
 :_Q_S_Q9 "Lesson Q9     | ! _ ? % + [ ]"
*:_Q_EXIT
#------------------------------------------------------------------------------
